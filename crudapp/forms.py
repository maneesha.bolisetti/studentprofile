from crudapp.models import Student
from .admin import StudentAdmin
from django import forms
class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = '__all__'