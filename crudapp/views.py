from django.shortcuts import render,redirect
from crudapp.models import Student
from crudapp.forms import StudentForm
# Create your views here.
def insert_view(request):
    form = StudentForm()
    if request.method == 'POST':
        form = StudentForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
    return render(request,'star/Insert.html',{'form':form})


def show_view(request):
    Students = Student.objects.all()
    return render(request,'star/show.html',{'Students':Students})

def delete_view(request,No):
    Students =Student.objects.get(No = No)
    Students.delete()
    return redirect('/')

